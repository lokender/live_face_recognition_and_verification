import time
import pickle
import cv2
import numpy as np
import tensorflow as tf
from scipy import misc

import detect_face
import facenet


# Face class to hold any detect face object
class Face:
    def __init__(self):
        self.name = None  # Name of the detected face
        self.bbox = None # bounding box of the detected face
        self.cropped_image = None # cropped face image

# Function to show details on the video
def show_on_video(image, detected_faces, fps):
    colors = [[255, 255, 0], [0, 255, 0], [0, 0, 255], [0,0, 255], [0, 255, 255], [255, 0, 255]]
    jk =0
    if detected_faces is not None:
        for face in detected_faces:
            bbt = face.bbox.astype(int)
            # Draw bounding box
            cv2.rectangle(image, (bbt[0], bbt[1]), (bbt[0] + (bbt[2] - bbt[0]), bbt[1] + (bbt[3] - bbt[1])),colors[jk], 3)
            if face.name is not None:
                # Put face name
                cv2.putText(image, face.name, (bbt[0] + (bbt[2] - bbt[0]) + 10, bbt[1] + (bbt[3] - bbt[1])), 0, 0.5, colors[jk],thickness=2, lineType=3)
            jk=jk+1
    _, w, _ = image.shape
    # Put fps
    cv2.putText(image, "Speed (fps): " + str(fps), (w-300, 30), cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 0, 255), thickness=2, lineType=2)


# Function to recognize the face give its embedding , mode and class_names
def recognize(embedding, model, class_names):
    if embedding is not None:
        # Predict
        predictions = model.predict_proba([embedding])
        # class with max prob
        best_class_indices = np.argmax(predictions, axis=1)
        # return class name
        return class_names[best_class_indices[0]]

def get_face_embedding(cropped_image, sess_embed):
    # Create input output tensor
    images_placeholder = tf.get_default_graph().get_tensor_by_name("input:0")
    embeddings = tf.get_default_graph().get_tensor_by_name("embeddings:0")
    phase_train_placeholder = tf.get_default_graph().get_tensor_by_name("phase_train:0")

    # Preprocess image
    mean = np.mean(cropped_image)
    std = np.std(cropped_image)
    std_adj = np.maximum(std, 1.0 / np.sqrt(cropped_image.size))
    cropped_image = np.multiply(np.subtract(cropped_image, mean), 1 / std_adj)

    # get image embedding
    feed_dict = {images_placeholder: [cropped_image], phase_train_placeholder: False}
    return sess_embed.run(embeddings, feed_dict=feed_dict)[0]

def get_faces(image, pnet, rnet, onet):
    detected_faces = []

    # face detection parameters
    min_face_size = 25  # minimum size of face
    thresh = [0.6, 0.7, 0.7]  # three steps's threshold
    factor = 0.709  # scale factor
    face_crop_size = 160
    face_crop_margin = 32

    face_bboxes, _ = detect_face.detect_face(image, min_face_size, pnet, rnet, onet, thresh, factor)

    for bb in face_bboxes:
        face = Face()
        face.bbox = np.zeros(4, dtype=np.int32)
        img_size = np.asarray(image.shape)[0:2]
        face.bbox[0] = np.maximum(bb[0] - face_crop_margin / 2, 0)
        face.bbox[1] = np.maximum(bb[1] - face_crop_margin / 2, 0)
        face.bbox[2] = np.minimum(bb[2] + face_crop_margin / 2, img_size[1])
        face.bbox[3] = np.minimum(bb[3] + face_crop_margin / 2, img_size[0])
        cropped = image[face.bbox[1]:face.bbox[3], face.bbox[0]:face.bbox[2], :]
        face.cropped_image = misc.imresize(cropped, (face_crop_size, face_crop_size), interp='bilinear')
        detected_faces.append(face)
    return detected_faces


def main():
    gpu_memory_fraction = 0.9

    facenet_model_checkpoint = "/mnt/softwares/acv_project_code/Code/Model/20170511-185253.pb"
    classifier_model = "/mnt/softwares/acv_project_code/Code/classfier_path/classifier_svm.pkl"

    after = 4 # Run after
    for_fps = 4  # report fps after
    fps = 0
    f_count = 0

    # Load classifier
    with open(classifier_model, 'rb') as infile:
        model, class_names = pickle.load(infile)

    # Load facenet model
    sess_embed = tf.Session()
    with sess_embed.as_default():
        facenet.load_model(facenet_model_checkpoint)

    # Get ready mtcnn for face detection
    with tf.Graph().as_default():
        gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction=gpu_memory_fraction)
        sess_face_det = tf.Session(config=tf.ConfigProto(gpu_options=gpu_options, log_device_placement=False))
        with sess_face_det.as_default():
            pnet, rnet, onet = detect_face.create_mtcnn(sess_face_det, None)


    # Main recognition algo starts from here
    #Capture Live  video
    video_capture = cv2.VideoCapture(0)

    # Start time for fps computation
    begin = time.time()
    while True:

        # Read Frame
        ret, frame = video_capture.read()
	frame = cv2.resize(frame, (320, 240))
        if (f_count % after) == 0:
            # Detect Faces
            detected_faces = get_faces(frame, pnet=pnet, rnet=rnet, onet=onet)

            # Iterate over all the faces detected in the current frame
            for i, face in enumerate(detected_faces):
                # Get embedding
                face_embedding = get_face_embedding(face.cropped_image, sess_embed=sess_embed)
                # Classify embedding/ face
                face.name = recognize(face_embedding, model=model,class_names=class_names)

            # Compute current FPS
            end = time.time()
            if (end - begin) > for_fps:
                fps= int(f_count/(end-begin))
                begin = time.time()
                f_count = 0

        # Show detected face on the live video along with FPS
        show_on_video(frame, detected_faces, fps)
        f_count += 1

        # Show live video
        cv2.imshow('ACV Live Face Recognition Project', frame)
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break

    # Release all
    video_capture.release()
    cv2.destroyAllWindows()


if __name__ == '__main__':
    main()

